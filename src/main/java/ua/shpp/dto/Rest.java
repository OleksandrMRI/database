package ua.shpp.dto;

public class Rest {
    private float rest;
    private int numberOfProduct;
    private int numberOfStore;


    public float getRest() {
        return rest;
    }

    public Rest setRest(float rest) {
        this.rest = rest;
        return this;
    }

    public int getNumberOfProduct() {
        return numberOfProduct;
    }

    public Rest setNumberOfProduct(int numberOfProduct) {
        this.numberOfProduct = numberOfProduct;
        return this;
    }

    public int getNumberOfStore() {
        return numberOfStore;
    }

    public Rest setNumberOfStore(int numberOfStore) {
        this.numberOfStore = numberOfStore;
        return this;
    }
}
