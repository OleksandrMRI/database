package ua.shpp.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class ProductDto {
    @Size(min = 10, max = 100, message = "Length of name should be >=7.")
    @Pattern(regexp = ".*b+.*", message = "Name should include one or more letter 'a'.")
    @NotNull
    private String productName;

    @Max(value = 35, message = "Size should be  between 0 and 24.")
    private int typeId;

    @Max(value = 1_000_000, message = "Value of rest should be less than 1_000_000.00.")
    private float price;

    public String getProductName() {
        return productName;
    }

    public ProductDto setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public int getTypeId() {
        return typeId;
    }

    public ProductDto setTypeId(int typeId) {
        this.typeId = typeId;
        return this;
    }

    public float getPrice() {
        return price;
    }

    public ProductDto setPrice(float price) {
        this.price = price;
        return this;
    }
}
