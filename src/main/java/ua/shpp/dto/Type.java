package ua.shpp.dto;

public class Type {
    String type_name;

    public Type(String type_name) {
        this.type_name = type_name;
    }

    public String getTypeName() {
        return type_name;
    }

    public Type setTypeName(String type_name) {
        this.type_name = type_name;
        return this;
    }
}
