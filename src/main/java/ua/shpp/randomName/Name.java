package ua.shpp.randomName;

import java.util.Random;

public class Name {
    private final int length;
    private final int CHAR_A = 65;
    private final int CHAR_a = 97;
    private final int ALPHABET_LENGTH = 25;
    private final StringBuilder sb = new StringBuilder();

    public Name(int length) {
        this.length = length;
    }

    public String createName(Random random) {
        int nameLength = random.nextInt(this.length);

        for (int i = 0; i <= nameLength; i++) {
            int caseType = random.nextBoolean() ? CHAR_A : CHAR_a;
            sb.append((char) (caseType + random.nextInt(ALPHABET_LENGTH)));
        }
        return sb.toString();
    }
}
