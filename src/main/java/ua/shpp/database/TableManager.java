package ua.shpp.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;

public class TableManager {
    static Logger log = LoggerFactory.getLogger(TableManager.class);
    public final String SQLEXCEPTION = "SQLException: ";
    Connection connection;
    BufferedReader br;

    public TableManager(Connection connection, BufferedReader br) {
        this.connection = connection;
        this.br = br;
    }

    public void createTables() {
        String sql = readFromFile();
        log.info("in create table");
        try (Statement statement = connection.createStatement()) {
            log.debug("{} connection", statement);
            statement.execute(sql);
        } catch (SQLException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
    }

    public void createRequest(String type) {
        log.info("in create request type equals: {}", type);
        String sqlQuery = readFromFile();
        long start = System.currentTimeMillis();
        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            statement.setString(1,type);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                log.info("Store: {}, {}; type: {}; rest: {}", resultSet.getString("store_name"),
                        resultSet.getString("store_address"), resultSet.getString("type_name"),
                        resultSet.getInt("sum_rest"));
            }
            long duration = System.currentTimeMillis() - start;
            log.info("Waiting for a response of store: {}ms", duration);
        } catch (SQLException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
    }

    private String readFromFile() {
        StringBuilder stringBuilder = new StringBuilder();
        String line="";

            while (true) {
                try {
                    if (!((line = br.readLine()) != null)) break;
                } catch (IOException e) {
                    log.error(SQLEXCEPTION, e);
                    System.exit(1);
                }
                stringBuilder.append(line).append("\n");
            }

        return stringBuilder.toString();
    }
}
