package ua.shpp.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.properties.PropertiesReader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

public class PrepareStatementCreator {
    private String SQLEXCEPTION = "SQLException: ";
    final Logger log = LoggerFactory.getLogger(Main.class);
    Connection connection;
    PropertiesReader propertiesReader;
    SqlRequestPojo sqlRequestPojo;
    public PrepareStatementCreator(Connection connection, PropertiesReader propertiesReader, SqlRequestPojo sqlRequestPojo) {
        this.connection=connection;
        this.propertiesReader=propertiesReader;
        this.sqlRequestPojo=sqlRequestPojo;
    }

    public void prepareStatementCreate(String sql) {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            if(Objects.equals(sql, sqlRequestPojo.getSqlProductInsertion())) {
                new ProductsTableFiller(statement, propertiesReader).fillProductsTable();
            }else if (Objects.equals(sql, sqlRequestPojo.getSqlStoresInsertions())){
                new StoreTableFiller(statement, propertiesReader).fillTableOfStores();
            }else if (Objects.equals(sql, sqlRequestPojo.getSqlTypesInsertion())){
                new TableTypesFiller(statement, propertiesReader).fillTableOfTypes();
            }else if (Objects.equals(sql, sqlRequestPojo.getSqlRestInsertion())){
                new RestTableFiller(statement, propertiesReader).fillRestTable();
            }
        } catch (SQLException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
    }
}
