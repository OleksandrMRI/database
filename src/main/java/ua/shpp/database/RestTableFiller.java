package ua.shpp.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.dto.Rest;
import ua.shpp.properties.PropertiesReader;

import java.sql.*;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class RestTableFiller {
    private final PreparedStatement statement;
    private final PropertiesReader propertiesReader;

    private final Logger log = LoggerFactory.getLogger(RestTableFiller.class);
    public final String SQLEXCEPTION = "SQLException: ";

    public RestTableFiller(PreparedStatement statement, PropertiesReader propertiesReader) {
        this.statement = statement;
        this.propertiesReader = propertiesReader;
    }

    public String fillRestTable() {
        int productNumbers = Integer.parseInt(propertiesReader.getProperty("nProducts"));
        int maxRest = Integer.parseInt(propertiesReader.getProperty("rest"));
        int storeNumbers = Integer.parseInt(propertiesReader.getProperty("nStores"));
        RandomNumbers randomNumbers = new RandomNumbers(new Random());
        long l = System.currentTimeMillis();
        AtomicInteger counter = new AtomicInteger();
        getGenerateToRests(productNumbers, storeNumbers, maxRest, randomNumbers)
                .forEach(p -> sendBatchToRests(statement, counter, p)
                );
        try {
            statement.executeBatch();
        } catch (SQLException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
        long result = (System.currentTimeMillis() - l);
        return "Filling products table duration: " + result / 1000L + ".\nSend field to rests-table per second: "
                + counter.intValue() * 1000L / result;
    }

    private Stream<Rest> getGenerateToRests(int productNumbers, int storeNumbers,
                                            int maxRest, RandomNumbers randomNumbers) {
        return Stream.generate(() -> new Rest().setRest(randomNumbers.getRandomFloat(maxRest))
                        .setNumberOfProduct(1)
                        .setNumberOfStore(randomNumbers.getRandomInt(storeNumbers)))
                .limit(productNumbers);
    }

    private void sendBatchToRests(PreparedStatement statement, AtomicInteger counter, Rest r) {
        try {
            counter.getAndIncrement();
            statement.setInt(1, counter.intValue());
            statement.setInt(2, r.getNumberOfStore());

            statement.setFloat(3, r.getRest());
            statement.addBatch();
            if (counter.intValue() % 1000 == 0) {
                statement.executeBatch();
                log.info("Next thousandth: {}", counter);
            }
        } catch (SQLException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
    }
}
