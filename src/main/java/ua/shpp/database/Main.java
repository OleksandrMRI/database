package ua.shpp.database;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.properties.PropertiesReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;


public class Main {
    static Logger log = LoggerFactory.getLogger(Main.class);
    private static final String SQLEXCEPTION = "SQLException: ";
    private static final String OUTSIDE_PROPERTIES = "config.properties";
    private static final String INSIDE_PROPERTIES = "conf/config.properties";
    private static final String inputFilePrimeScript = "src/main/resources/date.sql";
    private static final String outputFilePrimeScript = "sql_files/date.sql";
    private static final String inputFileForeignKeyScript = "src/main/resources/foreign_key.sql";
    private static final String outputFileForeignKeyScript = "sql_files/foreign_key.sql";
    private static final String inputFileRequestScript = "src/main/resources/request_script.sql";
    private static final String outputFileRequestScript = "sql_files/request_script.sql";


    public static void main(String[] args) {
        String type;
        if (args.length == 0) {
            type = "дім";
        } else {
            type = args[0];
            System.out.printf("\n" + type + "\n");

        }
        createConnection(type);
    }

    private static void createConnection(String type) {
        PropertiesReader propertiesReader = new PropertiesReader(new Properties(), OUTSIDE_PROPERTIES, INSIDE_PROPERTIES);
        String url = propertiesReader.getProperty("url");
        String user = propertiesReader.getProperty("user");
        String pass = propertiesReader.getProperty("pass");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, pass);
            connection.setAutoCommit(false);

            manageTables(connection, inputFilePrimeScript, outputFilePrimeScript);


            SqlRequestPojo sqlRequestPojo = new SqlRequestPojo();

            fillTables(propertiesReader, connection, sqlRequestPojo);

            manageTables(connection, inputFileForeignKeyScript, outputFileForeignKeyScript);


            manageRequest(connection, type);

            connection.commit();
            connection.close();
        } catch (SQLException e) {
            log.info("{}", connection);
            if (connection == null) {
                log.info("Connection null");
                System.exit(1);
            }
            try {
                connection.rollback();
            } catch (SQLException s) {
                log.error(SQLEXCEPTION, s);
                System.exit(1);
            }
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        } finally {
            if (connection == null) {
                System.exit(1);
            }
            try {
                connection.close();
            } catch (SQLException e) {
                log.info(SQLEXCEPTION, e);
                System.exit(1);
            }
        }
    }

    private static void manageTables(Connection connection, String input, String output) {
        File inputFile = new File(input);
        File outputFile = new File(output);
        if (inputFile.exists()) {
            invokeCreateTableMethods(connection, inputFile);
        } else {
            invokeCreateTableMethods(connection, outputFile);
        }
    }

    private static void invokeCreateTableMethods(Connection connection, File inputFile) {
        try (FileReader fileReader = new FileReader(inputFile, StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(fileReader)) {
            new TableManager(connection, br).createTables();
        } catch (IOException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
    }

    private static void manageRequest(Connection connection, String type) {
        File inputFile = new File(inputFileRequestScript);
        File outputFile = new File(outputFileRequestScript);
        if (inputFile.exists()) {
            invokeRequestMethod(connection, type, inputFile);
        } else {
            invokeRequestMethod(connection, type, outputFile);
        }
    }

    private static void invokeRequestMethod(Connection connection, String type, File inputFile) {
        try (FileReader fileReader = new FileReader(inputFile, StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(fileReader)) {
            new TableManager(connection, br).createRequest(type);
        } catch (IOException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
    }

    private static void fillTables(PropertiesReader propertiesReader, Connection connection, SqlRequestPojo sqlRequestPojo) {
        try (PreparedStatement statement = connection.prepareStatement(sqlRequestPojo.getSqlStoresInsertions());
             PreparedStatement statement1 = connection.prepareStatement(sqlRequestPojo.getSqlTypesInsertion());
             PreparedStatement statement2 = connection.prepareStatement(sqlRequestPojo.getSqlProductInsertion());
             PreparedStatement statement3 = connection.prepareStatement(sqlRequestPojo.getSqlRestInsertion())
        ) {
            new StoreTableFiller(statement, propertiesReader).fillTableOfStores();
            new TableTypesFiller(statement1, propertiesReader).fillTableOfTypes();
            String products = new ProductsTableFiller(statement2, propertiesReader).fillProductsTable();
            String rests = new RestTableFiller(statement3, propertiesReader).fillRestTable();
            log.info(products);
            log.info(rests);
        } catch (SQLException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
    }
}