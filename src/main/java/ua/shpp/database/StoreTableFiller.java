package ua.shpp.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.dto.Store;
import ua.shpp.properties.PropertiesReader;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class StoreTableFiller {
    private final String store = "Epicentr - ";
    private final String streetOfStore = "Vesela str., ";
    private int numberOfStore = 1;
    private final int maxRandomHouses = 200;
    private final PreparedStatement statement;
    private final PropertiesReader propertiesReader;
    private final Logger log = LoggerFactory.getLogger(Main.class);

    public final String SQLEXCEPTION = "SQLException: ";

    public StoreTableFiller(PreparedStatement statement, PropertiesReader propertiesReader) {
        this.statement = statement;
        this.propertiesReader = propertiesReader;
    }

    public void fillTableOfStores() {
        int n_stores = Integer.parseInt(propertiesReader.getProperty("nStores"));
        AtomicInteger updateCounts = new AtomicInteger();
        try {
            Stream.generate(() -> new Store().setStoreName(createStoreName()).setAddress(createAddress())).limit(n_stores)
                    .forEach(s ->
                    {
                        updateCounts.getAndIncrement();
                        try {
                            statement.setString(1, s.getStoreName());
                            statement.setString(2, s.getAddress());
                            statement.addBatch();
                        } catch (SQLException e) {
                            log.error(SQLEXCEPTION, e);
                            System.exit(1);
                        }
                    });
            statement.executeBatch();

        } catch (SQLException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
    }

    private String createAddress() {
        return streetOfStore + (new Random().nextInt(maxRandomHouses));
    }

    private String createStoreName() {
        return store + numberOfStore++;
    }
}
