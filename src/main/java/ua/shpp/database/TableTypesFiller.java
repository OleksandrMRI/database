package ua.shpp.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.properties.PropertiesReader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TableTypesFiller {
    PreparedStatement statement;
    PropertiesReader propertiesReader;
    private Logger log = LoggerFactory.getLogger(Main.class);
    public final String SQLEXCEPTION = "SQLException: ";

    public TableTypesFiller(PreparedStatement statement, PropertiesReader propertiesReader) {
        this.statement = statement;
        this.propertiesReader = propertiesReader;
    }

    public int[] fillTableOfTypes() {
        String[] types = propertiesReader.getProperty("types").split(",");
        int[] updateCounts = null;
        try {
            if (types.length == 0) {
                log.error("String[] types is empty");
                System.exit(1);
            } else {
                for (String type : types) {
                    statement.setString(1, type);
                    statement.addBatch();
                }
            }
            updateCounts = statement.executeBatch();
        } catch (SQLException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
        return updateCounts;
    }
}
