package ua.shpp.database;

import java.util.Random;

public class RandomNumbers {
    private final Random random;

    public RandomNumbers(Random random) {
        this.random = random;
    }

    public int getRandomInt(int n) {
        return 1 + random.nextInt(n);
    }

    public float getRandomFloat(int n) {
        return random.nextFloat() * n;
    }
}
