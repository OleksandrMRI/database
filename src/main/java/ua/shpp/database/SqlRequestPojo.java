package ua.shpp.database;

public class SqlRequestPojo {
    private String sqlStoresInsertions = "INSERT INTO stores (store_name, store_address) VALUES (?,?)";
    private String sqlProductInsertion = "INSERT INTO products (product_name, id_type) VALUES (?,?)";
    private String sqlRestInsertion = "INSERT INTO rests (id_product, id_store, rest) VALUES (?,?,?)";
    private String sqlTypesInsertion = "INSERT INTO product_types (type_name) VALUES (?)";

    public String getSqlStoresInsertions() {
        return sqlStoresInsertions;
    }

    public String getSqlProductInsertion() {
        return sqlProductInsertion;
    }

    public String getSqlRestInsertion() {
        return sqlRestInsertion;
    }

    public String getSqlTypesInsertion() {
        return sqlTypesInsertion;
    }

    public SqlRequestPojo setSqlStoresInsertions(String sqlStoresInsertions) {
        this.sqlStoresInsertions = sqlStoresInsertions;
        return this;
    }

    public SqlRequestPojo setSqlProductInsertion(String sqlProductInsertion) {
        this.sqlProductInsertion = sqlProductInsertion;
        return this;
    }

    public SqlRequestPojo setSqlRestInsertion(String sqlRestInsertion) {
        this.sqlRestInsertion = sqlRestInsertion;
        return this;
    }

    public SqlRequestPojo setSqlTypesInsertion(String sqlTypesInsertion) {
        this.sqlTypesInsertion = sqlTypesInsertion;
        return this;
    }
}
