package ua.shpp.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.dto.ProductDto;
import ua.shpp.properties.PropertiesReader;
import ua.shpp.randomName.Name;
import ua.shpp.violation.Violation;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class ProductsTableFiller {
    private final PreparedStatement statement;
    private final PropertiesReader propertiesReader;
    private final Logger log = LoggerFactory.getLogger(ProductsTableFiller.class);
    public final String SQLEXCEPTION = "SQLException: ";

    public ProductsTableFiller(PreparedStatement statement, PropertiesReader propertiesReader) {
        this.statement = statement;
        this.propertiesReader = propertiesReader;
    }

    public String fillProductsTable() {

        String strings = propertiesReader.getProperty("types");
        String[] types = strings.split(",");
        int numberOfTypes = types.length;
        int nameLength = Integer.parseInt(propertiesReader.getProperty("maxProductNameLength"));
        int nProducts = Integer.parseInt(propertiesReader.getProperty("nProducts"));
        Random random = new Random();
        RandomNumbers randomNumbers = new RandomNumbers(random);
        long l = System.currentTimeMillis();
        AtomicInteger counter = new AtomicInteger();
        Violation violation = new Violation();
        getGenerateToProducts(numberOfTypes, nameLength, randomNumbers, nProducts, violation, random)
                .forEach(p -> sendBatchToProducts(statement, counter, p)
                );
        try {
            statement.executeBatch();
        } catch (SQLException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }

        long result = (System.currentTimeMillis() - l);

        return  "Counter of valid products: " + counter + ".\nFilling products table duration: " + result / 1000L +
                ".\nSend fields to products-table per second: " + counter.intValue() * 1000L / result;
    }

    protected Stream<ProductDto> getGenerateToProducts(int numberOfTypes, int nameLength,
                                                       RandomNumbers randomNumbers, int nProducts,
                                                       Violation violation, Random random) {
        return Stream.generate(() -> new ProductDto().setProductName(new Name(nameLength).createName(random))
                        .setTypeId(randomNumbers.getRandomInt(numberOfTypes)))
                .filter(x -> violation.getViolation(x).isEmpty())
                .limit(nProducts);
    }

    private void sendBatchToProducts(PreparedStatement statement, AtomicInteger counter, ProductDto p) {
        try {
            counter.getAndIncrement();
            statement.setString(1, p.getProductName());
            statement.setInt(2, p.getTypeId());
            statement.addBatch();
            if (counter.intValue() % 1000 == 0) {
                statement.executeBatch();
                log.info("Thousand: {}", counter);
            }
        } catch (SQLException e) {
            log.error(SQLEXCEPTION, e);
            System.exit(1);
        }
    }
}
