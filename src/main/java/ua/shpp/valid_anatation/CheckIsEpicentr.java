package ua.shpp.valid_anatation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class CheckIsEpicentr implements ConstraintValidator<StartWithEpicentr, String> {
    String val;
    public void initialize(StartWithEpicentr store) {
        val = store.value();
    }

    @Override
    public boolean isValid(String store, ConstraintValidatorContext constraintValidatorContext) {
        return store.startsWith("Epicentr");
    }
}
