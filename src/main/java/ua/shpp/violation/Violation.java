package ua.shpp.violation;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import ua.shpp.dto.ProductDto;

import java.util.Set;

public class Violation {

    private static final ValidatorFactory VALIDATOR_FACTORY = Validation.buildDefaultValidatorFactory();
    private static final Validator VALIDATOR = VALIDATOR_FACTORY.getValidator();

    public Set<ConstraintViolation<ProductDto>> getViolation(ProductDto productDto) {
        return VALIDATOR.validate(productDto);
    }

}
