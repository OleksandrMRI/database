package ua.shpp.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertiesReader {
    private Properties properties;
    private String outsideProperties;
    private String insideProperties;
    private final Logger log = LoggerFactory.getLogger(PropertiesReader.class);

    public PropertiesReader(Properties properties, String outsideProperties, String insideProperties) {
        this.properties = properties;
        this.outsideProperties = outsideProperties;
        this.insideProperties = insideProperties;
    }


    public String getProperty(String key) {
        if (new File(outsideProperties).exists()) {
            log.debug("outsideProperties");
            loadProperties(outsideProperties);
        } else {
            loadProperties(insideProperties);
            log.debug("insideProperties");
        }
        String property = properties.getProperty(key);
        if (property == null) {
            log.error("Property \"{}\" does not exist", key);
            System.exit(1);
        }
        return properties.getProperty(key);
    }

    private void loadProperties(String fileName) {
        log.debug("fileName: {}", fileName);
        try (InputStream is = new FileInputStream(fileName);
             InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(isr)) {
            properties.load(br);
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException: ", e);
        } catch (IOException e) {
            log.error("IOException: ", e);
        }
    }
}