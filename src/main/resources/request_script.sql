SELECT type_name, store_name, store_address, sum(r.rest) as sum_rest
FROM products p, product_types t, stores s, rests r
WHERE p.id_type = t.id_type
  and s.id_store=r.id_store
  and r.id_product=p.id_product
  and t.type_name = ?
GROUP BY t.type_name, s.store_name, s.store_address
ORDER BY sum_rest desc
LIMIT 1;
