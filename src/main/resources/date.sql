DROP TABLE if exists rests;
DROP TABLE if exists products;
DROP TABLE if exists stores;
DROP TABLE if exists product_types;

CREATE TABLE stores
(
    id_store   serial      NOT NULL
        constraint PK_serial primary key,
    store_name varchar(15) NOT NULL,
    store_address varchar(20) NOT NULL
);

CREATE TABLE product_types
(
    id_type   serial      NOT NULL
        constraint PK_product_types primary key,
    type_name varchar(30) NOT NULL
);

CREATE TABLE products
(
    id_product   serial       NOT NULL
        constraint PK_products primary key,
    product_name varchar(100) NOT NULL,
    id_type      int          NOT NULL
);

CREATE TABLE rests
(
    id_rest serial   NOT NULL
        constraint PK_rest primary key,
    id_product   int      NOT NULL,
    id_store     smallint NOT NULL,
    rest         int8      NOT NULL default 0
);
