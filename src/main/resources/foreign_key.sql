ALTER TABLE products add constraint FK_products_product_types FOREIGN KEY (id_type) references product_types (id_type);
ALTER TABLE rests add constraint FK_rests_products FOREIGN KEY (id_product) references products (id_product);
ALTER TABLE rests add constraint FK_rests_stores FOREIGN KEY (id_store) references stores (id_store);
create index store_id_rest_index on rests (id_store);
create index type_id_product_index on products (id_type);